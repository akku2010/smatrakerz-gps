import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DashboardPage } from './dashboard';
import { TranslateModule } from '@ngx-translate/core';
import { ProgressBarComponent } from '../add-devices/progress-bar/progress-bar';
import { ProgressBarModule } from '../add-devices/progress-bar/progress-bar.module';
// import { ContentDrawerComponent } from '../../components/content-drawer/content-drawer';
// import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    DashboardPage,
    // ProgressBarComponent
    // ContentDrawerComponent
  ],
  imports: [
    IonicPageModule.forChild(DashboardPage),
    TranslateModule.forChild(),
    ProgressBarModule
    // ComponentsModule
  ],
  exports: [
    // ProgressBarComponent
    // ContentDrawerComponent
  ]
})
export class DashboardPageModule {}
